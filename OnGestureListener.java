/**
 * @以下方法中的参数解释：
 * @e1：第1个是 ACTION_DOWN MotionEvent 按下的动作
 * @e2：后一个是ACTION_UP MotionEvent 抬起的动作(这里要看下备注5的解释)
 * @velocityX：X轴上的移动速度，像素/秒
 * @velocityY：Y轴上的移动速度，像素/秒
 */
@Override
public boolean onDown(MotionEvent e) {
    // ACTION_DOWN
    v_str.add("onDown");
    return false;
}
 
@Override
// ACTION_DOWN 、短按不移动
public void onShowPress(MotionEvent e) {
    v_str.add("onShowPress");
 
}
 
@Override
// ACTION_DOWN 、长按不滑动
public void onLongPress(MotionEvent e) {
    v_str.add("onLongPress");
}
 
@Override
// ACTION_DOWN 、慢滑动
public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
        float distanceY) {
    v_str.add("onScroll");
    return false;
}
 
@Override
// ACTION_DOWN 、快滑动、 ACTION_UP
public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
        float velocityY) {
    v_str.add("onFling");
    // -------备注5----------
    // if(e1.getAction()==MotionEvent.ACTION_MOVE){
    // v_str.add("onFling");
    // }else if(e1.getAction()==MotionEvent.ACTION_DOWN){
    // v_str.add("onFling");
    // }else if(e1.getAction()==MotionEvent.ACTION_UP){
    // v_str.add("onFling");
    // }
    // if(e2.getAction()==MotionEvent.ACTION_MOVE){
    // v_str.add("onFling");
    // }else if(e2.getAction()==MotionEvent.ACTION_DOWN){
    // v_str.add("onFling");
    // }else if(e2.getAction()==MotionEvent.ACTION_UP){
    // v_str.add("onFling");
    // }
    if (isChagePage)
        bmp = BitmapFactory.decodeResource(getResources(),
                R.drawable.himi_dream);
    else
        bmp = BitmapFactory.decodeResource(getResources(),
                R.drawable.himi_warm);
    isChagePage = !isChagePage;
    return false;
}
 
@Override
// 短按ACTION_DOWN、ACTION_UP
public boolean onSingleTapUp(MotionEvent e) {
    v_str.add("onSingleTapUp");
    return false;
}
